# Uso y documentación

# BACKEND
Backend realizado utilizando springboot + Java 8 + H2 In memory database

## Decisiones de diseño
Con respecto a la manera de realizar la prueba, se pensó hacerlo de la mejor manera para mantener el codigo facil de leer, entender e integrar con nuevas funcionalidades.

### Arquitectura
El backend esta diseñado siguiendo una arquitectura basada en capas, para de esta forma mantener bajo acoplamiento y hacer el codigo más entendible y facil de leer a la hora de agregar nuevas funcionalidades. Las capaz planteadas son las siguientes y se encuentran en sus respectivos paquetes en Java.
- Capa de acceso a datos: Esta capa es la que se encuentra a más bajo nivel y es la que se encarga de comunicar el backend con la base de datos H2, se representa en el paquete de "Repository" y "Model"
- Capa de servicios: Esta capa se encuentra a un nivel intermedio y se encarga de establecer los servicios que prestará el sistema por cada uno de los modelos definidos, esta se comunica con la capa de acceso a datos y realiza las operaciones necesarias para cada operacion. 
- Capa de controladores: Esta capa es la que se encuentra a más alto nivel, y se encarga de exponer el endopint REST provisto por el aplicativo y manejar las peticiones del usuario.
- Capa transversal (sidecar): Esta capa es transversal a todo el aplicativo (Front-end y Back-end), es la informacion que se transporta entre uno y otro, para el caso de la prueba los registros del usuario.

### Patrones de diseño

Se siguieron patrones de diseño basicos para realizar el aplicativo.
- Patron fachada: Se utiliza este patron para reducir el acoplamiento entre las capaz, para ello se crean interfaces o "fachadas" independientes de su implementación como es el caso de los servicios
- Patron singleton: Utilizando las convenciones de Spring, se utiliza el patron singleton con la anotacion @AutoWired para tener una unica instancia de un objeto mientras el programa esta en ejecución estos objeton estan marcados con las anotaciones (@Service, @Bean) 
- SOLID: Se intentan mantener los principios de desarrollo SOLID al momento de creación del proyecto

## Endpoints definidos

Debido a la simplicidad del proyecto y conforme a las recomendaciones de diseño para servicios REST, se tiene un único endpoint para las operaciones principales y se diferencian por el verbo HTTP con el que se accede. 
### Uso de cada endpoint

A continuación, se establece la relacion del endpoint con cada operación

> https://localhost:8080/api/v1/entries/

| Verbo HTTP | Descripción                    |
| ------------- | ------------------------------ |
| `GET /`      | El GET Basico sin parámetros devuelve una lista de todos los registros creados       |
| `GET /?processed=`      |Utilizando el parámetro processed y utilizando valores `true` o `false` se obtiene la lista de registros procesados o sin procesar|
| `POST/`   | Crea un nuevo registro mediante un JSON con los datos `name`, `lastName` y `processed`, este ultimo es opcional    |
| `PATCH/{id,list}`   | Utilizando este método, se marcan como procesados los registros dentro de la lista de id provista E.j PATCH /1,2,3  |

## Detalles técnicos

Por defecto el aplicativo es un proyecto Maven desarrollado utilizando eclipse y el JDK 8.

# FRONT-END
El front-end del aplicativo se realizó utilizando js y html basico
## Decisiones de diseño
### Arquitectura

Siendo parte del mismo sistema, el frontend es una capa del sistema completo, proveyendo la vista de todo el sistema a manera de capa independiente.

### Patrones de diseño
A pesar de no utilizar ningún framework a la hora de realizar la prueba, se pensó en una organización de carpetas que logrará mantener el codigo poco acoplado, los archivos de codigo fuente con una extension corta y facilidad de integración ya gregación.

### Estructura del proyecto
Se crearon los siguientes modulos de javascript utilizando la sintaxis de ES6.
- api: En el modulo api, se escribieron las funciones que mediante el API de fetch permite al usuario realizar peticiones al backend. Este modulo es utilil para evitar repeticion de codigo en cada uno de los llamados.
- util: Este modulo contiene funciones utilitarias que pueden ser utilizadas por otros modulos sin generar una gran dependencia entre estos mismos
- constans: Modulo que se encarga de exportar las constantes utiles dentro del proyecto

## Manual de uso

Para correr el front-end, solo hace falta cualquier servidor estatico tales como nginx o apache referenciando la carpeta raiz `web-site` y el archivo index como index1.html. En mi caso, utilice nginx y un archivo de configuración como el que se ve a continuación.


        server {
        	listen 80;
	        server_name localhost;
        	location / {
            	autoindex on;
	            root /user/path/to/dir;
	            index index1.html index1.htm;
        }
    }
    
	

La interfaz de usuario es sencilla, y se trata de 4 pestañas con las funcionalidades solicitadas.
- En la primera pestaña, se encuentra el formulario que permite crear nuevos registros.
- En la segunda pestaña, se encuentran solo aquellos registros que no se marcaron como procesados a la hora de crearse, en esta misma pantalla, se encuentra el botón "Procesar", que permite marcar como procesados los registros seleccionados
- En la tercera pestaña se encuentran aquellos registros marcados como procesados.
- En la cuarta pestaña estan todos los registros.

Los datos se cargan de manera dinamica cada vez que el usuario entra o sale de una pestaña, para mantener su vista siempre actualizada con respecto a los datos almacenados en la base de datos
## Detalles técnicos
El front-end fue desarrollado utilizando la sintaxis de ES6, Html 5 y MUI CC para estilizar algunos botones y formularios.
