package co.prueba.valid.dto.mapper;

import co.prueba.valid.dto.model.UserEntryDTO;
import co.prueba.valid.model.FormEntry.UserEntry;

public class UserEntryMapper {
	public static UserEntryDTO toUserEntryDTO( UserEntry userEntry) {
		return new UserEntryDTO()
				.setFullName(userEntry.getFirstName() + " " + userEntry.getLastName())
				.setEntryId(userEntry.getId())
				.setProcessedState(userEntry.getProcessed());
	}
}
