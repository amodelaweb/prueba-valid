package co.prueba.valid.dto.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
public class UserEntryDTO {
	private Integer entryId;
    private String fullName;
    private Boolean processedState;
}
