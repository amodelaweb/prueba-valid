package co.prueba.valid.service.implementation;

import co.prueba.valid.dto.mapper.UserEntryMapper;
import co.prueba.valid.dto.model.UserEntryDTO;
import co.prueba.valid.model.FormEntry.UserEntry;
import co.prueba.valid.repository.UserEntryRepository;
import co.prueba.valid.service.UserEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserEntryServiceImpl implements UserEntryService {

	@Autowired
	private UserEntryRepository userEntryRepository;

	@Override
	public void createUserEntry(String name, String lastName, Boolean processed) {
		Boolean processedState = processed != null ? processed : Boolean.FALSE;
		UserEntry userEntry = new UserEntry().setFirstName(name).setLastName(lastName)
				.setProcessed(processedState);

		userEntryRepository.save(userEntry);
	}

	@Override
	public List<UserEntryDTO> getUserEntries(Optional<Boolean> processed) {
		if (processed.isPresent())
			return userEntryRepository.findAllByProcessed(processed.get()).stream()
					.map(userEntry -> UserEntryMapper.toUserEntryDTO(userEntry)).collect(Collectors.toList());

		return userEntryRepository.findAll().stream().map(userEntry -> UserEntryMapper.toUserEntryDTO(userEntry))
				.collect(Collectors.toList());
	}

	@Override
	public boolean markEntryAsProcessed(Integer entryId) {
		boolean result = false;
		Optional<UserEntry> userEntry = userEntryRepository.findById(entryId);
		if (userEntry.isPresent()) {
			if (userEntry.get().getProcessed() != Boolean.TRUE) {
				userEntry.get().setProcessed(Boolean.TRUE);
				userEntryRepository.save(userEntry.get());
				result = true;
			}
		}
		return result;
	}

}
