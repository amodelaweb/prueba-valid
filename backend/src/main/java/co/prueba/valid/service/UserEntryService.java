package co.prueba.valid.service;

import co.prueba.valid.dto.model.UserEntryDTO;

import java.util.List;
import java.util.Optional;

public interface UserEntryService {

    void createUserEntry (String name, String lastName, Boolean processed);

    List<UserEntryDTO> getUserEntries(Optional<Boolean> processed);

    boolean markEntryAsProcessed(Integer entryId);
}
