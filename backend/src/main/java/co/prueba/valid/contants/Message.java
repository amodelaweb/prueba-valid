package co.prueba.valid.contants;

import lombok.Getter;

@Getter
public enum Message {

    CREATED_ENTRY("A new entry was created!"),
    MARKED_OR_NOT_FOUND("The entry does not exist or is processed!"),
    ENTRY_PROCESSED("The entry is processed!");

    private String message;

    Message(String message) {
        this.message = message;
    }
}
