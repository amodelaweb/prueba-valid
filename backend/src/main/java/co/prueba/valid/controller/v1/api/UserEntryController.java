package co.prueba.valid.controller.v1.api;

import co.prueba.valid.contants.Message;
import co.prueba.valid.controller.v1.request.UserEntryRequest;
import co.prueba.valid.dto.response.Response;
import co.prueba.valid.service.UserEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/entries")
public class UserEntryController {

	@Autowired
	private UserEntryService userEntryService;

	@GetMapping
	public ResponseEntity getUserEntries(@RequestParam Optional<Boolean> processed) {
		return ResponseEntity.ok(Response.ok().setPayload(userEntryService.getUserEntries(processed)));
	}

	@PostMapping
	public ResponseEntity createUserEntity(@RequestBody @Valid UserEntryRequest request) {
		userEntryService.createUserEntry(request.getName(), request.getLastName(), request.getProcessed());
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(Response.created().setPayload(Message.CREATED_ENTRY.getMessage()));
	}

	@PatchMapping("/{idList}")
	public ResponseEntity processNewUserEntry(@PathVariable List<Integer> idList) {
		HttpStatus status = HttpStatus.OK;
		List<String> messages = new ArrayList<>();
		
		for (Integer id : idList) {
			if (!userEntryService.markEntryAsProcessed(id)) {
				status = HttpStatus.CONFLICT;
				messages.add("The entry with id " + id.toString() + " doesn't exist or it was processed before.");
			} else {
				messages.add("The entry with id " + id.toString() + " was processed successfully!");
			}
		}
		
		if (messages.isEmpty()) {
			status = HttpStatus.NO_CONTENT;
			messages.add("The wasn't any id to process");
		}

		return ResponseEntity.status(status).body(Response.genericResponse().setStatus(status).setPayload(messages));
	}
}
