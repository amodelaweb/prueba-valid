package co.prueba.valid.controller.v1.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Optional;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
public class UserEntryRequest {

    @NotEmpty(message = "First name is required!")
    private String name;

    @NotEmpty(message = "Last name is required!")
    private String lastName;
    
    private Boolean processed;

}
