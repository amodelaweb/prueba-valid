package co.prueba.valid.repository;

import co.prueba.valid.model.FormEntry.UserEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserEntryRepository extends JpaRepository<UserEntry, Integer> {
    List<UserEntry> findAllByProcessed(boolean processed);
}
