export const clearPanelChildren = (panel_id) => {
    let element = document.getElementById(panel_id);
    while (element.firstChild) {
        element.firstChild.remove();
    }
}

export const constructTwoColumnTable = (data, containerId) => {
    let bigContainer = document.getElementById(containerId);
    let table = document.createElement("table");
    table.classList.add("mui-table");
    table.classList.add("mui-table--bordered");
    let thead = document.createElement("thead");
    let trHead = document.createElement("tr");
    
    let firstTh = document.createElement("th");
    firstTh.appendChild(document.createTextNode("Registro"));
    trHead.appendChild(firstTh)
    
    let secondTh = document.createElement("th");
    secondTh.appendChild(document.createTextNode("Seleccionar"));
    trHead.appendChild(secondTh);

    thead.appendChild(trHead)
    let tblBody = document.createElement("tbody");

    data.forEach(element => {
        let trBody = document.createElement("tr");
        let entryDatatd = document.createElement("td");
        let entryDataText = document.createTextNode(element.fullName);
        entryDatatd.appendChild(entryDataText);
        let entrySelectNode = document.createElement("td");
        let entrySelectNodeInput = document.createElement("input");
        entrySelectNodeInput.type = "checkbox";
        entrySelectNodeInput.id = `el+${element.entryId}`;
        entrySelectNode.appendChild(entrySelectNodeInput);
        trBody.appendChild(entryDatatd);
        trBody.appendChild(entrySelectNode);
        tblBody.appendChild(trBody);
    });

    table.appendChild(thead);
    table.appendChild(tblBody);

    bigContainer.appendChild(table);
}

export const constructOneColumnTable = (data, containerId) => {
    let bigContainer = document.getElementById(containerId);
    let table = document.createElement("table");
    table.classList.add("mui-table");
    table.classList.add("mui-table--bordered");
    let thead = document.createElement("thead");
    let trHead = document.createElement("tr");
    
    let firstTh = document.createElement("th");
    firstTh.appendChild(document.createTextNode("Registro"));
    trHead.appendChild(firstTh)

    thead.appendChild(trHead)
    let tblBody = document.createElement("tbody");

    data.forEach(element => {
        let trBody = document.createElement("tr");
        let entryDatatd = document.createElement("td");
        let entryDataText = document.createTextNode(element.fullName);
        entryDatatd.appendChild(entryDataText);
        trBody.appendChild(entryDatatd);
        tblBody.appendChild(trBody);
    });

    table.appendChild(thead);
    table.appendChild(tblBody);

    bigContainer.appendChild(table);
}

const convertStringIdToNumericId = (stringId) => {
    let replaceStringPart = stringId.replace("el+", "");
    return parseInt(replaceStringPart);
}

export const getAllCheckedData = (selectorId) => {
    let containerTable = document.getElementById(selectorId).firstElementChild;
    let checkedElementsArray = []

    for (let row of containerTable.rows) {
        let td = row.cells[1];
        if (td.tagName !== "TH") {
            let inputKid = td.firstElementChild;
            if (inputKid.checked){
                let numericId = convertStringIdToNumericId(inputKid.id);
                checkedElementsArray.push(numericId);
            }
        }
    }
    
    return checkedElementsArray;
}