const headers = {
    'Content-type': 'application/json'
}

const handleResponse = (response) => (response.text().then(text => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
        return Promise.reject(data);
    }
    return data.payload;
}));

export const apiGet = (url, params = '') => () => fetch(`${url}${params}`, {
    method: 'GET',
    headers: new Headers(headers)
}).then(v => handleResponse(v));

export const apiPost = (url, obj) => () => fetch(`${url}`, {
    method: 'POST',
    body: JSON.stringify(obj),
    headers: new Headers(headers)
}).then(v => handleResponse(v));

export const apiPatch = (url, id='') => () => fetch(`${url}${id}`, {
    method: 'PATCH',
    headers: new Headers(headers)
}).then(v => handleResponse(v))