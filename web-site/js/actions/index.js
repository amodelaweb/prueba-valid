import {urlEntries} from '../api/urls/index.js';
import {apiPost, apiGet, apiPatch} from '../api/index.js';
import {constructTwoColumnTable, constructOneColumnTable, clearPanelChildren} from '../util/index.js';

export const createRegister = (requestbody) => {
    apiPost(urlEntries, requestbody)().then(response => alert("El registro se creo exitosamente !"), error => alert("Hubo un error creando el registro!"))
}

export const getEntriesByCategory = (is_processed, containerId) => {
    let params = is_processed !== null
        ? `?processed=${is_processed}`
        : '';
    apiGet(urlEntries,params)().then(
        entries => {  
            if (is_processed !== null && is_processed === false) {
                constructTwoColumnTable(entries, containerId);
            }else{
                constructOneColumnTable(entries, containerId);
            }
        },
        error => alert("Error al obtener informacion del servidor!")
    )
}

export const markEntriesAsProcessed = (entriesToBeProcessed) => {
    let arrayToString = entriesToBeProcessed.join(',');
    apiPatch(urlEntries,arrayToString)().then(
        success => {
            clearPanelChildren('notProcessedPanel');
            getEntriesByCategory(false, 'notProcessedPanel');
        },
        error => {
            alert("Error al procesar algunos datos!");    
            clearPanelChildren('notProcessedPanel');
            getEntriesByCategory(false, 'notProcessedPanel');
        }
    )
}