import {createRegister, getEntriesByCategory, markEntriesAsProcessed} from './actions/index.js';
import {clearPanelChildren, getAllCheckedData} from './util/index.js';

function createRegisterEvent () {
    let nameOnForm = document.getElementById('firstName').value;
    let lasNameOnForm = document.getElementById('lastName').value;
    let processedOnForm = document.getElementById('processedCheck').checked ? true : false; 
    let entryObject = {
        name: nameOnForm,
        lastName: lasNameOnForm,
        processed: processedOnForm
    }
    createRegister(entryObject);
}

function getNonProcessedEntriesEvent () {
    clearPanelChildren('notProcessedPanel');
    getEntriesByCategory(false, 'notProcessedPanel');
}

function getProcessedEntriesEvent () {
    clearPanelChildren('processedPanel');
    getEntriesByCategory(true, 'processedPanel');
}

function getAllEntriesEvent () {
    clearPanelChildren('allPanel');
    getEntriesByCategory(null, 'allPanel');
}

function getAllCheckedDataEvent () {
    let checkedArray = getAllCheckedData('notProcessedPanel');
    console.log(checkedArray)
    if (Array.isArray(checkedArray) && checkedArray.length !== 0){
        markEntriesAsProcessed(checkedArray);
    }else{
        alert("Primero debe seleccionar al menos un valor!");
    }
}

//Buttons
document.getElementById('createRegistryButton').addEventListener('click', createRegisterEvent);
document.getElementById('processSelectedData').addEventListener('click', getAllCheckedDataEvent);

//Tabs
document.getElementById('loadNonProcessedData').addEventListener('click', getNonProcessedEntriesEvent);
document.getElementById('loadProcessedData').addEventListener('click', getProcessedEntriesEvent);
document.getElementById('loadAllData').addEventListener('click', getAllEntriesEvent);